//DEBUG - odpiąć 5V z arduino i wpiąć USB
//RELEASE - wpiąć 5V do płytki

#include <Adafruit_NeoPixel.h>
#include <SimpleTimer.h>
#include <math.h>
#include <SoftwareSerial.h>

#define RxD 0
#define TxD 1
#define BUZZER 2
#define DEFIBRILLATOR 3
#define DIODES_PIN 4
#define DIODES_AMOUNT 2
#define ADRENALINE 5
#define WALLS 6
#define TRAPSTOMACH 7
#define TRAPLUNGS 8
#define TRAPKIDNEY 9
#define TRAPPANCREAS 10
#define TRAPLIVER 11
#define TRAPINTESTINES 12
#define PROXIMITY 7

int health = 100;
int blinkThreadId = -1;
int decreaseHPThreadId = -1;
int buzzerThreadId = -1;
int eyesThreadId = -1;
int defibrillatorThreadId = -1;
int reviveThreadId = -1;
int proximityTrapThreadId = -1;
int diePermamentlyThreadId = -1;
int adrenalineThreadId = -1;
int wallCheckThreadId = -1;
int BTThreadId = -1;
int difficulty = 0;
int adrenaline = 0;
int chances = 0;
bool revivable = false;
bool reviving = false;
bool adrenalinePushed = false;
bool wallTouched = false;
bool isBTInputValid = false;
bool proximityTooClose = false;

SoftwareSerial BTSerial(RxD, TxD); //bluetooth
 
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(DIODES_AMOUNT, DIODES_PIN, NEO_GRB + NEO_KHZ800);
SimpleTimer timer;
 
void setup()
{
  pixels.setPixelColor(0, 0, 0, 0);
  pixels.setPixelColor(1, 0, 0, 0);
  pixels.show();
  pinMode(ADRENALINE, INPUT);
  pinMode(DEFIBRILLATOR, INPUT);
  pinMode(BUZZER, OUTPUT);
  pinMode(WALLS, INPUT);
  pinMode(TRAPSTOMACH, INPUT);
  pinMode(TRAPLUNGS, INPUT);
  pinMode(TRAPKIDNEY, INPUT);
  pinMode(TRAPPANCREAS, INPUT);
  pinMode(TRAPLIVER, INPUT);
  pinMode(TRAPINTESTINES, INPUT);
  pixels.begin(); // Inicjalizacja biblioteki diod
  //game(3);//do DEBUG
  //Serial.begin(9600);//do DEBUG
  BTSerial.begin(9600);
  BTThreadId = timer.setInterval(100, SendData);
}
 
void loop()
{
  do { // do while moze blokowac arduino!
    byte BTInput;
    if ( BTSerial.available() == 0){
      break;
    }
    BTInput = BTSerial.read(); 
    switch(BTInput)
    {
      case 'h':
      {
        game(1);
        isBTInputValid = true;
        break;
      }
      case 'm':
      {
        game(2);
        isBTInputValid = true;
        break;
      }
      case 'e':
      {
        game(3);
        isBTInputValid = true;
        break;
      }
      case 'x':
      {
        DiePermamently();
        isBTInputValid = true;
        break;
      }
      case 'a':
      {
        Adrenaline();
        isBTInputValid = true;
        break;
      }
    }
  } while (isBTInputValid);
  timer.run();
}

void SendData()
{
  //Serial.print("health: ");
  //Serial.println(health);
  //Serial.print("TRAPSTOMACH: ");
  //Serial.println(digitalRead(TRAPSTOMACH));
  //Serial.print("TRAPLUNGS: ");
  //Serial.println(digitalRead(TRAPLUNGS));
  //Serial.print("TRAPKIDNEY: ");
  //Serial.println(digitalRead(TRAPKIDNEY));
  //Serial.print("TRAPPANCREAS: ");
  //Serial.println(digitalRead(TRAPPANCREAS));
  //Serial.print("TRAPLIVER: ");
  //Serial.println(digitalRead(TRAPLIVER));
  //Serial.print("TRAPINTESTINES: ");
  //Serial.println(digitalRead(TRAPINTESTINES));
  //Serial.print("walls: ");
  //Serial.println(digitalRead(WALLS));
  //Serial.print("proximity: ");
  //Serial.println(analogRead(PROXIMITY));
  BTSerial.print('#');
  BTSerial.print(health);
  BTSerial.print("_");
  BTSerial.print(digitalRead(TRAPLIVER));
  BTSerial.print("_");
  BTSerial.print(digitalRead(TRAPSTOMACH));
  BTSerial.print("_");
  BTSerial.print(digitalRead(TRAPPANCREAS));
  BTSerial.print("_");
  BTSerial.print(digitalRead(TRAPLUNGS));
  BTSerial.print("_");
  if(proximityTooClose) //zblizenie
  {
    BTSerial.print("1");
  }
  else
  {
    BTSerial.print("0");
  }
  BTSerial.print("_");
  BTSerial.print(digitalRead(TRAPINTESTINES));
  BTSerial.print("_");
  BTSerial.print(digitalRead(TRAPKIDNEY));
  BTSerial.print("_");
  BTSerial.print(digitalRead(WALLS));
  BTSerial.print("_");
  BTSerial.print(String(adrenaline));//andrenalina
  BTSerial.print("_");
  BTSerial.print(String(difficulty-chances));//defibrylacje
  BTSerial.print("_");
  BTSerial.print(String(
    (TRAPLIVER+TRAPSTOMACH+TRAPPANCREAS+TRAPLUNGS)%2) //itd
    );//pRZYSTOSC
}

void game(int _difficulty)
{
  timer.deleteTimer(decreaseHPThreadId);
  timer.deleteTimer(buzzerThreadId);
  timer.deleteTimer(eyesThreadId);
  timer.deleteTimer(defibrillatorThreadId);
  timer.deleteTimer(adrenalineThreadId);
  timer.deleteTimer(wallCheckThreadId);
  difficulty = _difficulty;
  health = 100*difficulty;
  adrenaline = 0;
  chances = difficulty;
  defibrillatorThreadId = timer.setInterval(10, Revive);
  RunTimers();
}

void RunTimers()
{
  timer.deleteTimer(diePermamentlyThreadId);
  eyesThreadId = timer.setInterval(1000,Eyes);
  timer.deleteTimer(blinkThreadId);
  blinkThreadId=-1;
  RunBuzzer();
  decreaseHPThreadId = timer.setInterval(1000, DecreaseHP);
  proximityTrapThreadId = timer.setTimeout(min(health*10*2,2000),ProximityTrap);
  adrenalineThreadId = timer.setInterval(100, Adrenaline);
  wallCheckThreadId = timer.setInterval(100, WallCheck);
}

void ProximityTrap()
{
  int proximity = analogRead(PROXIMITY);
  if(proximity<400||proximity>600)
  {
    health = health-5;
    proximityTooClose = true;
  }
  else
  {
    proximityTooClose = false;
  }
  if(health>0)
  {
    proximityTrapThreadId = timer.setTimeout(min(health*10*2,2000),ProximityTrap);
  }
}

void Adrenaline()
{
  if(digitalRead(ADRENALINE)==LOW && health>0)
  {
    if(adrenalinePushed == false)
    {
      health = health + (difficulty*100)/3;
      adrenaline = adrenaline + 1;
      timer.deleteTimer(blinkThreadId);
      blinkThreadId=-1;
      adrenalinePushed = true;
    }
  }
  else
  {
    adrenalinePushed = false;
  }
}

void Revive()
{

  //Serial.println(digitalRead(DEFIBRILLATOR));
  if(digitalRead(DEFIBRILLATOR)==HIGH)
  {
    timer.disable(defibrillatorThreadId);
    timer.setTimeout(1000, DefibrillatorLag);
    if(health>0 && reviving == false)
    {
      DiePermamently();
      reviving=true;
      chances = 0;
    }
    else
    {
      if(reviving == false && revivable == true)
      {
        health = 50;
        reviving = true;
        RunTimers();
      }
    }
  }
  else
  {
    reviving = false;
  }
}

void Eyes()
{
  float HPCoef = health/((100.0f*difficulty)*(1+adrenaline/3.0f));
  int r = min(255, -510 * HPCoef +510);
  int g = min(255, 510 * HPCoef);
  int b = 0;

  if(health>10)
  {
    pixels.setPixelColor(0, g*0.1, r*0.1, b*0.1);//po zamontowaniu zmienić mnożnik
    pixels.setPixelColor(1, g*0.1, r*0.1, b*0.1); 
    pixels.show();
  }
  else
  {
    if(blinkThreadId==-1)
    {
      BlinkOn();
    }
  } 
}

void RunBuzzer()
{
  digitalWrite(BUZZER,HIGH);
  buzzerThreadId = timer.setTimeout(min(health*10,1000),BuzzerOff);
}

void BuzzerOff()
{
  digitalWrite(BUZZER,LOW);
  if(health>0)
  {
    buzzerThreadId=timer.setTimeout(min(health*10,1000), RunBuzzer);
  }
  else
  {
    DeadBuzzerOn();
  }
}

void DeadBuzzerOn()
{
  digitalWrite(BUZZER,HIGH);
  buzzerThreadId=timer.setTimeout(3000, DeadBuzzerOff);
}

void DeadBuzzerOff()
{
  digitalWrite(BUZZER,LOW);
}

void BlinkOn()
{
    pixels.setPixelColor(0, 0, 255*0.1, 0);//po zamontowaniu zmienic mnoznik
    pixels.setPixelColor(1, 0, 255*0.1, 0); 
    pixels.show();
    blinkThreadId=timer.setTimeout(health*100,BlinkOff);
}

void BlinkOff()
{
    pixels.setPixelColor(0, 0, 0, 0);
    pixels.setPixelColor(1, 0, 0, 0);
    pixels.show();
    if(health>0)
    {
      blinkThreadId=timer.setTimeout(health*100,BlinkOn);
    }
}

void DecreaseHP()
{
  health=health-1-adrenaline;
  if(health<=0 && chances>0)
  {
    revivable = true;
    timer.deleteTimer(decreaseHPThreadId);
    diePermamentlyThreadId = timer.setTimeout(5000, DiePermamently);
    chances = chances - 1;
  }
}

void DiePermamently()
{
  health=0;
  revivable = false;
  BlinkOff();
  timer.deleteTimer(decreaseHPThreadId);
}

void DefibrillatorLag()
{
  timer.enable(defibrillatorThreadId);
}

void WallCheck()
{
  if(digitalRead(WALLS)==LOW)
  {
    health=health-1;
  }
}
